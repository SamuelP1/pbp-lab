class Post {
  final String title;
  final String body;
  final int views;
  // for now
  final String source;

  Post(this.title, this.body, this.views, this.source);
}
