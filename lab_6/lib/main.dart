import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lab_6/models/post.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);
  var post = Post(
      'This is the title',
      "This is the body, it's not too long.\nHas 2 lines at least.",
      20,
      'https://google.com');

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Ngingetin',
      theme: ThemeData(),
      home: Scaffold(
          appBar: AppBar(
            title: const Text('Content'),
            centerTitle: true,
            backgroundColor: Colors.black,
          ),
          body: Container(
            padding: const EdgeInsets.fromLTRB(20, 50, 20, 50),
            child: Row(
              children: [
                Row(
                  children: [
                    Column(children: [
                      Text(
                        post.title,
                        style:
                            const TextStyle(color: Colors.white, fontSize: 18),
                      ),
                      Text('Views: ${post.views}',
                          style: const TextStyle(
                              color: Colors.white, fontSize: 18)),
                      ElevatedButton(
                        onPressed: () => {},
                        child: const Text('READ'),
                        style: ElevatedButton.styleFrom(primary: Colors.grey),
                      )
                    ]),
                  ],
                ),
              ],
            ),
            color: const Color(0xF0000000),
          )),
    );
  }
}
