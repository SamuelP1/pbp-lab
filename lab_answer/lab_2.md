Questions:
1. Apakah perbedaan antara JSON dan XML?
2. Apakah perbedaan antara HTML dan XML?

Answers:
1. JSON & XML merupakan dua syntax untuk menyimpan dan mengirim data. Tapi, kedua hal tersebut memiliki beberapa perbedaan di antara mereka. Pertama, data dalam JSON file disimpan dalam bentuk dictionary (tipenya seperti punya python tapi mungkin dari programming language juga), tapi data dalam XML file disimpan dengan format seperti html (menggunakan tags dan lainnya). Selain itu, menggunakan XML lebih aman dibandingkan menggunakan JSON. Akan tetapi, menggunakan JSON lebih mudah dan lebih cepat (untuk beberapa situasi) dibandingkan menggunakan XML.

2. Walaupun kedua HTML dan XML menggunakan tags, mereka mempunyai peran yang sangat berbeda. HTML digunakan untuk menampilkan data kepada user (di browser), sedangkan xml digunakan untuk menyimpan dan mengirimkan data (untuk diproses bukan untuk dilihat user). Selain itu, XML akan bekerja dengan sama saat pengguna mengunakan platform / browser apapun, tapi hal tersebut tidak berlaku untuk HTML.

Sources:
slides minggu 4 Data Delivery 
https://www.geeksforgeeks.org/difference-between-json-and-xml/
https://www.upgrad.com/blog/html-vs-xml/#:~:text=HTML%20and%20XML%20are%20related,language%20that%20defines%20other%20languages.