from django import forms
from lab_1.models import Friend

class FriendForm(forms.ModelForm):

    class Meta:
        model = Friend
        fields = ["name", "npm", "dob"]
    
    error_messages = {
        "required" : "Please Type"
    }

    name_attrs = {
        "type": "text",
        "placeholder" : "Nama Friend",
    }

    npm_attrs = {
        "type" : "text",
        "placeholder" : "Npm Friend",
    }

    dob_attrs = {
        "type" : "date",
    }

    name = forms.CharField(label="", required=False, max_length=50, widget=forms.TextInput(attrs= name_attrs))
    npm = forms.CharField(label="", required=False, max_length=10, widget=forms.TextInput(attrs= npm_attrs))
    dob = forms.DateField(label="", required=False, widget= forms.DateInput(dob_attrs))
