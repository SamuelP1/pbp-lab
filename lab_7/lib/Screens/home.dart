import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Lab 7!'),
        backgroundColor: Colors.black,
        centerTitle: true,
      ),
      body: Container(
        child: const FormThing(),
        color: const Color(0xF0000000),
        alignment: Alignment.topLeft,
      ),
    );
  }
}

class FormThing extends StatefulWidget {
  const FormThing({Key? key}) : super(key: key);

  @override
  _FormThingState createState() => _FormThingState();
}

class _FormThingState extends State<FormThing> {
  final GlobalKey _formKey = GlobalKey<FormState>();
  var string = "NO POST SELECTED!";
  bool? isCheck = false;
  bool? isCheckAgain = false;

  fakeNavigateTo(int id) {
    setState(() {
      string = "WE ARE GOING TO POST WITH ID: $id";
    });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          const Text(
            "PLEASE CLICK ON ONE POST ONLY",
            style: TextStyle(color: Colors.white, fontSize: 25),
          ),
          Expanded(
              child: CheckboxListTile(
            title: const Text(
              'POST TITLE 1',
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            subtitle: const Text(
              'VIEWS: 20',
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            value: isCheck,
            activeColor: Colors.red,
            selectedTileColor: Colors.blue,
            onChanged: (value) {
              setState(() {
                isCheck = value;
              });
            },
          )),
          Expanded(
              child: CheckboxListTile(
            title: const Text(
              'POST TITLE 2',
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            subtitle: const Text(
              'VIEWS: 20000',
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            value: isCheckAgain,
            activeColor: Colors.red,
            selectedTileColor: Colors.blue,
            onChanged: (value) {
              setState(() {
                isCheckAgain = value;
              });
            },
          )),
          ElevatedButton(
            child: const Text(
              "Submit",
              style: TextStyle(color: Colors.white),
            ),
            onPressed: () {
              if (isCheck != isCheckAgain) {
                if (isCheck == true) {
                  fakeNavigateTo(1);
                } else if (isCheckAgain == true) {
                  fakeNavigateTo(2);
                }
              }
            },
          ),
          Text(
            string,
            style: const TextStyle(color: Colors.white, fontSize: 20),
          )
        ],
      ),
    );
  }
}
