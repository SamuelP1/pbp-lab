from django.db import models
from django.db.models.fields import CharField

# Create your models here.
class Note(models.Model):
    to = CharField(max_length=50)
    origin = CharField(max_length=50)
    title = CharField(max_length=100)
    message = CharField(max_length=1000)