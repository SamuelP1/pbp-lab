from django import forms
from lab_2.models import Note

class NoteForm(forms.ModelForm):

    class Meta:
        model = Note
        fields = ["to", "origin", "title", "message",]

    error_messages = {
        "required" : "Please type"
    }

    message_widget = {
        "class" : "long_message",
        "autocomplete" : "off",
    }

    title_widget = {
        "id" : "long_input",
        "autocomplete" : "off",
    }

    normal_widget = {
        "autocomplete" : "off",
    }

    to = forms.CharField(label="", required=True, max_length=50, widget=forms.TextInput(attrs=normal_widget))
    origin = forms.CharField(label="", required=True, max_length=50, widget=forms.TextInput(attrs=normal_widget))
    title = forms.CharField(label="", required=True, max_length=100, widget= forms.TextInput(attrs=title_widget))
    message = forms.CharField(label="", required=True, max_length=1000, widget= forms.Textarea(attrs= message_widget))
