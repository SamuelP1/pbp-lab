from django.urls import path
from lab_4.views import add_note, index, note_list

urlpatterns = [
    path("", index, name= "lab4_index"),
    path("add-note/", add_note, name="lab4_add"),   
    path("note-list/", note_list, name="lab4_note_list"),
]