$(document).ready(function() {

    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    // get table
    function getTable() {
        $.ajax({
            url : "http://localhost:8000/lab-2/json/",
            type : "GET",
            dataType : "json",
            success : function(result) {
                const jsonTable = result;
                console.log(jsonTable);
            }
        });
    }

    $(".view").click(function() {
        const id = $(this).attr("id").substring(4);

        $.ajax({
            url : "http://localhost:8000/lab-5/" + id,
            type : "GET",
            dataType : "JSON",
            success : function(result) {
                console.log(result)
                result = result[0];
                result = result.fields;
                $("#view-div").show();
                $("#edit-div").hide();
                $("#to").text("To: " + result.to);
                $("#from").text("From:" + result.origin);
                $("#title").text("Title: " + result.title);
                $("#message").text("Message: " + result.message);
            }
        });
    });

    $(".edit").click(function() {
        const id = $(this).attr("id").substring(4);
        console.log(id);

        $("#view-div").hide();
        $("#edit-div").show();

        $("#submit-button").click(function() {
            const to = $("#to-input").val();
            const from = $("#from-input").val();
            const title = $("#title-input").val();
            const message = $("#message-input").val();
            var csrftoken = getCookie('csrftoken');

            $.ajax({
                url : "http://localhost:8000/lab-5/" + id + "/update",
                type : "POST",
                data : {
                    to : to,
                    origin : from,
                    title : title,
                    message : message,
                    csrfmiddlewaretoken : csrftoken,
                },
                dataType : "json",
                success : function(result) {
                    result = result.response;
                    var data = JSON.parse(result);
                    console.log(data);
                    data = data[0].fields;
                    $("#to" + id).text(data.to);
                    $("#from" + id).text(data.origin);
                    $("#title" + id).text(data.title);
                    $("#message" + id).text(data.message);
                    
                }
            });
        });
    });

    $(".delete").click(function() {
        const id = $(this).attr("id").substring(6);
        console.log(id);
        const csrftoken = getCookie("csrftoken");

        $.ajax({
            url : "http://localhost:8000/lab-5/" + id + "/delete",
            type : "POST",
            data : {
                "csrfmiddlewaretoken" : csrftoken
            },
            dataType : "json",
            success : function() {
                $("#row" + id).empty();
                $("#row" + id).remove();
                $("#row" + id).remove();
            }
        });
    });


});