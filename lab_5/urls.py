from django.urls import path
from .views import get_note, index, update_note, delete_note

urlpatterns = [
    path("", index, name="lab5_index"),
    path("<id>", get_note, name="lab5_get_note"),
    path("<id>/update", update_note, name="lab5_update_note" ),
    path("<id>/delete", delete_note, name="lab5_delete_note" ),
]