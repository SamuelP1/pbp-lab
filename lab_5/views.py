from django.http.response import HttpResponse, JsonResponse
from django.shortcuts import render
from lab_2.models import Note
from django.core import serializers

from lab_5.forms import NoteForm

# Create your views here.
def index(request):
    notes = Note.objects.all().values()
    response = {
        "notes" : notes 
    }
    return render(request, "lab5_index.html", response)

def get_note(request, id):
    data = serializers.serialize("json", Note.objects.filter(id = id))
    return HttpResponse(data, content_type="application/json")

def update_note(request,id):
    if request.is_ajax() and request.method == "POST":
        selected = Note.objects.get(id=id)
        form = NoteForm(request.POST, instance=selected)

        if form.is_valid():
            selected = form.save()
            response = serializers.serialize("json", [selected, ])
            return JsonResponse({"response" : response}, status =200)
        else :
            return JsonResponse({"error" : ""}, status = 404)
    return JsonResponse({"error" : ""}, status = 404)

def delete_note(request, id):
        if request.is_ajax() and request.method == "POST":
            selected = Note.objects.get(id=id)
            selected.delete()
            return JsonResponse({"nothing" : ""}, status = 200)
        return JsonResponse({"nothing" : ""}, status = 404)
